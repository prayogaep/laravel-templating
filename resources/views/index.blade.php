{{-- <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Media Online</title>
</head> --}}
@extends('layouts.main')

@section('content')
    <div class="container">
        <h1><b>Media Online</b></h1>
        <h2><b>Social Media Developer</b></h2>
        <p>Belajar dan Berbagi agar hidup menjadi lebih baik</p>
        <h2><b>Benefit Join di Media Online</b></h2>
        <ul>
            <li>Mendapatkan motivasi dari sesama developer</li>
            <li>Sharing knowledge dari para mastah Sanber</li>
            <li>Dibuat oleh calon web developer terbaik</li>
        </ul>

        <h2><b>Cara Bergabung ke Media Online</b></h2>
        <ol>
            <li>Mengunjungi Website ini</li>
            <li>Mendaftar di <a href="/register">Form Sign up</a></li>
            <li>Selesai</li>
        </ol>
    </div>
@endsection
