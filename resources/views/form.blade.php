@extends('layouts.main')

@section('content')
    <div class="container">
        <h1><b>Buat Account Baru!</b></h1>
        <h2><b>Sign Up Form</b></h2>
        <form action="/welcome" method="post">
            @csrf
            <label for="firstname">First name:</label><br>
            <br>
            <input type="text" name="firstname"><br>
            <br>
            <label for="lastname">Last name:</label><br>
            <br>
            <input type="text" name="lastname"> <br>
            <br>
            <label for="gender">Gender:</label> <br>
            <br>
            <input type="radio" id="male" name="fav_language" value="Male">
            <label for="male">Male</label><br>
            <input type="radio" id="Female" name="fav_language" value="Female">
            <label for="Female">Female</label><br>
            <input type="radio" id="other" name="fav_language" value="Others">
            <label for="other">Others</label>
            <br>
            <br>
            <label for="nationality">Nationality:</label> <br>
            <br>
            <select name="nationality" id="nationality">
                <option value="Indonesia">Indonesia</option>
                <option value="Malaysia">Amerika</option>
                <option value="Singapore">Inggris</option>
            </select>
            <br>
            <br>
            <label for="language">Language Spoken: </label> <br>
            <br>
            <input type="checkbox" id="bahasaindonesia" name="bahasaindonesia" value="Bahasa Indonesia">
            <label for="bahasaindonesia"> Bahasa Indonesia</label><br>
            <input type="checkbox" id="english" name="english" value="English">
            <label for="english"> English</label><br>
            <input type="checkbox" id="other" name="other" value="Other">
            <label for="other"> Other</label>
            <br>
            <br>
            <label for="bio">Bio:</label>
            <br>
            <br>
            <Textarea rows="10" cols="30"></Textarea><br>
            <button type="submit">Sign Up</button>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>

        </form>
    </div>
@endsection
