@extends('layouts.main')

@section('content')
    <h1>Show Cast id : {{ $cast->id }}</h1>
    <div class="card" style="width: 18rem;">
        <div class="card-body">
            <h5 class="card-title"> Nama Cast : {{ $cast->nama }}</h5> <br>
            <h5 class="card-title"> Umur : {{ $cast->umur }}</h5>
            <p class="card-text">Bio : {{ $cast->bio }}</p>
            <a href="/cast" class="btn btn-primary">Kembali</a>
        </div>
    </div>
@endsection
