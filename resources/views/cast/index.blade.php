@extends('layouts.main')

@section('content')
    <div class="container">
        <h1> Daftar Cast </h1>
        <a href="/cast/create" class="btn btn-primary mb-3">Tambah Cast</a>
        @if (session()->has('success'))
            <div class="alert alert-success col-lg-10 mb-3" role="alert">
                {{ session('success') }}
            </div>
        @endif
        <div class="row">
            <div class="col-lg-10">
                <div class="card">
                    <div class="card-body">
                        <table class="table" id="example1">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Nama</th>
                                    <th scope="col">Umur</th>
                                    <th scope="col">Bio</th>
                                    <th scope="col">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($cast as $c)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $c->nama }}</td>
                                        <td>{{ $c->umur }}</td>
                                        <td>{{ $c->bio }}</td>
                                        <td>
                                            <a href="/cast/{{ $c->id }}" class="btn btn-info">Detail</a>
                                            <a href="/cast/{{ $c->id }}/edit" class="btn btn-warning text-white">Ubah</a>
                                            <form action="/cast/{{ $c->id }}" method="post" class="d-inline">
                                                @method('delete')
                                                @csrf
                                                <button type="submit" class="btn btn-danger">Hapus</button>
                                            </form>
                                        </td>
                                    </tr>
                                @empty
                                    <td colspan="5" class="text-center">Belum Ada Data</td>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>

    <script>
        $(function() {
            $("#example1").DataTable();
        });
    </script>
@endpush
