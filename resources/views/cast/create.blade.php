@extends('layouts.main')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <h1>Form tambah cast</h1>
                <form action="/cast" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="nama">Nama</label>
                        <input type="text" class="form-control" id="nama" name="nama" placeholder="Masukan Nama Cast..">
                    </div>
                    <div class="form-group">
                        <label for="umur">Umur</label>
                        <input type="number" min="0" class="form-control" id="umur" name="umur" placeholder="Masukan Umur Cast..">
                    </div>
                    <div class="form-group">
                        <label for="bio">Bio</label>
                        <textarea class="form-control" name="bio" id="bio" rows="3"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Simpan Data</button>
                </form>
            </div>
        </div>
    </div>
@endsection
